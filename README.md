![feeb-logo.png](https://bitbucket.org/repo/deyKn6/images/3224742589-feeb-logo.png)

### feebit-service ###
* @host: 104.236.152.177
* @port: 8081
* @URI: /api/feebit

### Endpoints ###
* @host:@port/@URI/rate?score=99&weight=1.0
* @host:@port/@URI/show

### Technologies ###
* Java 1.7 or greater
* Maven
* GIT
* Bitbucket
* Spring (boot)
* IntelliJ

### Installation ###
1. Install Java, Maven, GIT, IntelliJ
2. Clone feebit-services to your workspace
3. Build using native maven (mvn package)
4. Optionally, download feebit-service-1.0-SNAPSHOT.jar from [bitbucket.com](https://bitbucket.org/albertvijai/feebit-service/src/0b35a19fc8291e30e614f25301ba08b4d6e22f59/target/?at=master)
5. Run using ** java -jar /your/fatjar/location/feebit-service-1.0-SNAPSHOT.jar**

### Deployment ###
* Hot swap the fat jar to **//104.236.152.177/home/feebit_runtime**
* Restart Jar only if there is a non java change

### Examples ###
* http://104.236.152.177:8080/user/check 
* http://104.236.152.177:8080/settings/dev_token

### Other ###
* Project design details, account credentials, etc... refer your box.com/feebit/_tech