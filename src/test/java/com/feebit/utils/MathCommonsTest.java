package com.feebit.utils;

import junit.framework.TestCase;
import org.junit.Test;

public class MathCommonsTest extends TestCase {

    MathCommons mathCommons;

    double[] feebitArray =
            { 95.6, 85.6, 96.5, 84.2, 54.9, 74.5, 81.0,  81.2, 90.3, 77.3, 64.1,  59.9, 72.2, 92.0, 82.1, 51.0,
                    79.8, 11.0, 10.0,  78.8, 90.0, 92.3 };//22

    double[] feebitWeightArray =
            {  1.5,  0.8,  1.2,  0.4,  0.8,  1.8,  1.2,  1.1,  1.0,  0.7, 1.3,  0.6,  0.7,  1.3,  0.7,  1.0,  0.4,
                    0.1,  1.4,  0.9, 1.1,  0.3 };

    double[] identicalWeightsArray =
            {  0.5,  0.5,  0.5,  0.5,  0.5,  0.5,  0.5,  0.5,  0.5,  0.5, 0.5,  0.5,  0.5,  0.5,  0.5,  0.5,  0.5,
                    0.5,  0.5,  0.5, 0.5,  0.5 };

    double[] unitWeightsArray =
            {  1.0,  1.0,  1.0,  1.0,  1.0,  1.0,  1.0,  1.0,  1.0,  1.0, 1.0,  1.0,  1.0,  1.0,  1.0,  1.0,  1.0,
                    1.0,  1.0,  1.0, 1.0,  1.0 };

    public void setUp() throws Exception {
        super.setUp();
        mathCommons = new MathCommons();
    }



    @Test
    public void testGetWeightedMean() throws Exception {
        System.err.println(mathCommons.getWeightedMean(feebitArray, feebitWeightArray));
    }
}