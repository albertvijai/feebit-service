package com.feebit.repository;

import com.feebit.App;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@WebAppConfiguration
@ComponentScan("com.feebit")
@EnableAutoConfiguration
public class UserRepositoryTest extends TestCase {

    @Autowired
    UserRepository repo;

    @Test
    public void testDelete(){
        repo.deleteByEmailAddress("test@feebit.com");

    }


}