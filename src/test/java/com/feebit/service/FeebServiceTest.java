package com.feebit.service;

import com.feebit.App;
import com.feebit.domain.Feeb;
import com.feebit.domain.FeebEnum;
import junit.framework.TestCase;
import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@EnableAutoConfiguration
@Configuration
public class FeebServiceTest extends TestCase {

    @Autowired
    FeebService feebService;
    Feeb feeb;


    @Before
    public void setUp() throws Exception {
        feeb = new Feeb(new ObjectId(), new ObjectId());
    }

    @Test
    public void testCreateFeeb() throws Exception {
        feeb.setFeebMethod(FeebEnum.FeebMethod.C2F);
        feeb.setFeebPct(85.00);
        feeb.setClientPct(95.00);
        feebService.createFeeb(feeb);
    }

    @Test
    public void testFindFeeb() throws Exception {
        feeb.setId(new ObjectId("54f226f4f5431dfadae6a53a"));
        feebService.findFeeb(feeb);
        assertTrue(feeb.getId().equals(new ObjectId("54f226f4f5431dfadae6a53a")));
    }

    public void testSaveOrUpdateFeeb() throws Exception {

    }

    public void testDeleteFeeb() throws Exception {

    }
}