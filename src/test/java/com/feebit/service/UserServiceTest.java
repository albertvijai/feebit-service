package com.feebit.service;

import com.feebit.App;
import com.feebit.domain.FeebEnum;
import com.feebit.domain.Profile;
import com.feebit.domain.User;
import junit.framework.TestCase;
import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@EnableAutoConfiguration
@Configuration
public class UserServiceTest extends TestCase {

    @Autowired
    UserService userService;

    private User user;
    private Profile profile;
    private String emailAddress;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        emailAddress = "entity@feebit.com";
//        if(null!=userService.findUser(emailAddress))
//            userRepo.deleteByEmailAddress(emailAddress);
//
//        userService.createUser(emailAddress);
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testCreateUser() throws Exception {
        assertEquals(emailAddress, userService.createUser(emailAddress).getEmailAddress());
    }

    @Test
    public void testCreateMultipleUser() throws Exception {
        for(int i=0;i<25;i++)
            userService.createUser(Integer.toString(i));
    }


    @Test
    public void testFindUser() throws Exception {
        assertEquals(emailAddress, userService.findUser(emailAddress).getEmailAddress());
    }

    @Test
    public void testSaveOrUpdateUser() throws Exception {
        user = userService.findUser(emailAddress);
        user.setDisplayName("feebitTest");
        assertEquals(user.getDisplayName(), userService.saveOrUpdateUser(user).getDisplayName());
    }

    @Test
    public void testCreateProfile() throws Exception {
        user = userService.findUser(emailAddress);
        profile = new Profile();
        profile.setUserId(user.getId());
        profile.setProfileType(FeebEnum.ProfileType.PERSON);
        profile = userService.createProfile(profile);

        assertEquals(user.getId(), profile.getUserId());
    }

    //@Test
    public void testFindProfile() throws Exception {
        profile = new Profile();
        profile.setId(new ObjectId("54f21430f5433e70badf0ed6"));
        profile = userService.findProfile(profile);
        assertEquals("54f21430f5433e70badf0ed6", profile.getId().toString());
    }

    @Test
    public void testFindProfiles() throws Exception {
        user = userService.findUser(emailAddress);
        List<Profile> profiles = userService.findProfiles(user);
        for(Profile profile: profiles){
            System.err.println("---->>  "+profile.getId());
        }
    }

    //@Test
    public void testSaveOrUpdateProfile()throws Exception {
        user = userService.findUser("entity@feebit.com");
        List<Profile> profiles = userService.findProfiles(user);
        for(Profile aProfile: profiles){
            aProfile.setAbout("I UPDATED THIS PROFILE @ "+new Date().toString());
            userService.saveOrUpdateProfile(aProfile);
        }
    }

    //@Test
    public void testDeleteProfile()throws Exception {
        profile = new Profile();
        profile.setId(new ObjectId("54ececc7962c76783c209877"));
        profile.setUserId(new ObjectId("54ecd805962c4925af1ff2c2"));
        userService.deleteProfile(profile);
    }

}