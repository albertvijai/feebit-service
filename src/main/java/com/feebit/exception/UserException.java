package com.feebit.exception;

/**
 * Created by Albert Vijai on 2/18/2015.
 */
public class UserException extends Exception{

    public UserException(String message) {
        super(message);
    }

}
