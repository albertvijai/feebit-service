package com.feebit.exception;

/**
 * Created by Albert Vijai on 2/23/2015.
 */
public class RatingException extends Exception{

    public RatingException(String message) {
        super(message);
    }
}
