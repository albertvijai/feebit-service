package com.feebit.exception;

/**
 * Created by Albert Vijai on 2/27/2015.
 */
public class FeebException extends Exception{

    public FeebException(String message) {
        super(message);
    }
}
