package com.feebit.utils;


import org.apache.commons.math3.stat.descriptive.SynchronizedDescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.moment.Mean;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Albert Vijai on 2/1/2015.
 */
public class MathCommons {

    //Thread safe descriptive statistics
    SynchronizedDescriptiveStatistics statistic = null;


    public SynchronizedDescriptiveStatistics createStats(double[] values){
        statistic = new SynchronizedDescriptiveStatistics();
        List valuesList = Arrays.asList(values);
        if(null!=valuesList && !valuesList.isEmpty()){
            for(Double aValue: values){
                statistic.addValue(aValue);
            }
        }
        return statistic;
    }


    //weighted arithmetic mean
    public Double getWeightedMean(double[] values, double[] weights){
        Mean weightedMean = new Mean();
        return weightedMean.evaluate(values, weights);
    }






}
