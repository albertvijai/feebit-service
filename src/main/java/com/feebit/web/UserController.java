package com.feebit.web;

import com.feebit.domain.User;
import com.feebit.repository.UserRepository;
import com.feebit.utils.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Albert Vijai on 1/2/2015.
 */

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    JSONParser jParser = new JSONParser();

    @RequestMapping("/check")
    public String checkUserService(){
        String retVal = "[]";
        User aUser = null;
        List<User> users = userRepository.findAll();// TODO: Add pagable later..
        if(!users.isEmpty()){
            aUser = users.get(0);
            retVal = jParser.parseObj(aUser);
        }
        return retVal;
    }

}
