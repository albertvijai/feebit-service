package com.feebit.web;

import com.feebit.domain.Settings;
import com.feebit.repository.SettingsRepository;
import com.feebit.utils.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Albert Vijai on 1/2/2015.
 */

@RestController
@RequestMapping("/api/settings")
public class SettingsController {

    @Autowired
    private SettingsRepository settingsRepository;

    JSONParser jParser = new JSONParser();

    @RequestMapping("/key/{key}")
    public String getSettingsByKey(@PathVariable("key") String key) {
        System.err.println(key);
        String retVal = "[]";
        Settings setting = settingsRepository.findByKey(key);// TODO: Add pagable later..
        if(null!=setting)
            retVal = jParser.parseObj(setting);
        return retVal;
    }


    @RequestMapping("/value/{value}")
    public String getSettingsByValue(@PathVariable("value") String value) {
        System.err.println(value);
        String retVal = "[]";
        Settings setting = settingsRepository.findByValue(value);// TODO: Add pagable later..
        if(null!=setting)
            retVal = jParser.parseObj(setting);
        return retVal;
    }

}
