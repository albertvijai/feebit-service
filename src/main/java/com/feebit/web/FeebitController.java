package com.feebit.web;

import com.feebit.domain.Feeb;
import com.feebit.domain.FeebEnum;
import com.feebit.exception.FeebException;
import com.feebit.service.FeebService;
import com.feebit.utils.JSONParser;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by albertvmanoharan on 1/28/15.
 */
@RestController
@RequestMapping("/api/feebit/")
public class FeebitController {

    Feeb feeb;
    ObjectId clientId, feebId;

    @Autowired
    FeebService feebService;
    JSONParser jsonParser = new JSONParser();

    @RequestMapping("{method}/{from}/{to}/{score}")
    public String testPostFeeb(@PathVariable("method") String method, @PathVariable("from") String from, @PathVariable("to") String to, @PathVariable("score") String score){
        try{
            //client: 54f2272ef543f6096b94f47c
            //feeb  : 54f2272ef543f6096b94f47d
            //localhost:8081
            ///api/feebit/C2F/54f2272ef543f6096b94f47c/54f2272ef543f6096b94f47d/86.00
            ///api/feebit/F2C/54f2272ef543f6096b94f47d/54f2272ef543f6096b94f47c/29.00
            if(method.equals(FeebEnum.FeebMethod.C2F.toString())){
                clientId = new ObjectId(from);
                feebId = new ObjectId(to);
                feeb = new Feeb(clientId, feebId);
                feeb.setFeebPct(Double.parseDouble(score));
            }
            else {//F2C
                clientId = new ObjectId(to);
                feebId = new ObjectId(from);
                feeb = new Feeb(clientId, feebId);
                feeb.setClientPct(Double.parseDouble(score));
            }
            feeb = feebService.createFeeb(feeb);
        }
        catch (FeebException feebEx){
            feebEx.printStackTrace();
        }
        return jsonParser.parseObj(feeb);
    }

}
