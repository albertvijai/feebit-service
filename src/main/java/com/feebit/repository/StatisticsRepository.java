package com.feebit.repository;

import com.feebit.domain.Statistics;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Albert Vijai on 2/27/2015.
 */
@Repository
public interface StatisticsRepository extends MongoRepository<Statistics, String> {
}
