package com.feebit.repository;

import com.feebit.domain.Settings;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Albert Vijai on 1/4/2015.
 */
@Repository
public interface SettingsRepository extends MongoRepository<Settings, String> {

    Settings findByKey(String key);
    Settings findByValue(String value);

}
