package com.feebit.repository;

import com.feebit.domain.Feeb;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Albert Vijai on 2/27/2015.
 */
@Repository
public interface FeebRepository extends MongoRepository<Feeb, String> {

    Feeb findById(ObjectId id);

}
