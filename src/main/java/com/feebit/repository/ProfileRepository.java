package com.feebit.repository;

import com.feebit.domain.Profile;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by 913573 on 2/24/2015.
 */

@Repository
public interface ProfileRepository extends MongoRepository<Profile, String> {

    Profile findById(ObjectId id);


}
