package com.feebit.repository;

/**
 * Created by Albert Vijai on 1/2/2015.
 */

import com.feebit.domain.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    User findById(String id);
    User findById(ObjectId id);
    User findByEmailAddress(String emailAddress);
    Long deleteByEmailAddress(String emailAddress);

}
