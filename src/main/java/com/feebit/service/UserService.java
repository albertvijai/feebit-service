package com.feebit.service;

import com.feebit.domain.FeebEnum;
import com.feebit.domain.Profile;
import com.feebit.domain.User;
import com.feebit.exception.UserException;
import com.feebit.repository.ProfileRepository;
import com.feebit.repository.UserRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Albert Vijai on 1/2/2015.
 */
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    ProfileRepository profileRepository;

    User user;
    Profile profile;

    public User findUser(String emailAddress){
        return userRepository.findByEmailAddress(emailAddress);
    }

    public User findUserById(String id){
        return userRepository.findById(id);
    }

    public User findUserById(ObjectId id){ return userRepository.findById(id); }

    public User createUser(String emailAddress)throws UserException{
        if(null==findUser(emailAddress)){
            user = new User(emailAddress);
            userRepository.save(user);
            createProfile(createDefaultProfile(user));
        }
        else
            throw new UserException("User already exit");

        return user;
    }


    private Profile createDefaultProfile(User user){
        profile = new Profile();
        profile.setUserId(user.getId());
        profile.setProfileType(FeebEnum.ProfileType.DEFAULT);
        return profile;
    }

    public User saveOrUpdateUser(User user)throws UserException{//Non Cascade save.
        this.user = findUser(user.getEmailAddress());
        if(user!=null){
            user.setId(this.user.getId());
            userRepository.save(user);
        }
        return user;
    }

    public Profile createProfile(Profile profile) throws UserException{

        profile.setEnabled(false);//Only user can enable profile.
        user = findUserById(profile.getUserId());
        profileRepository.save(profile);//Save the document in DB.
        profile.addToUser(user);
        userRepository.save(user);//Add the reference in user.

        return profile;
    }

    public Profile findProfile(Profile profile)throws UserException{
        return profileRepository.findById(profile.getId());
    }

    public Profile findProfile(ObjectId profileId)throws UserException{
        return profileRepository.findById(profile.getId());
    }

    public List<Profile> findProfiles(User user)throws UserException{
        return  userRepository.findById(user.getId()).getProfiles();
    }


    public Profile saveOrUpdateProfile(Profile profile)throws UserException{
        this.profile = profileRepository.findById(profile.getId());
        if(this.profile.getId().equals(profile.getId()) && this.profile.getUserId().equals(profile.getUserId()))
            this.profile = profileRepository.save(profile);//Save on User/Profile Match
        return this.profile;
    }

    public void deleteProfile(Profile profile)throws UserException{
        user=userRepository.findById(profile.getUserId());
        if(null!=user.getProfiles() && !user.getProfiles().isEmpty()){
            Profile dbProfile = null;
            Iterator<Profile> profilesItr = user.getProfiles().iterator();
            while(profilesItr.hasNext()){
                dbProfile = profilesItr.next();
                if(dbProfile.getId().equals(profile.getId())){
                    profilesItr.remove();
                    profileRepository.delete(dbProfile.getId().toString());//delete profile
                }
            }
        }
        userRepository.save(user);//remove profile dbRef
    }

}
