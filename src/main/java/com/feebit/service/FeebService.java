package com.feebit.service;

import com.feebit.domain.Feeb;
import com.feebit.exception.FeebException;
import com.feebit.repository.FeebRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Albert Vijai on 2/27/2015.
 */
@Service
public class FeebService {

    @Autowired
    FeebRepository feebRepository;

    private Feeb feeb;

    public Feeb createFeeb(Feeb feeb) throws FeebException{
        if(null==feeb.getClientProfileId() || null==feeb.getFeebProfileId())
            throw new FeebException("Cannot link feeb. provide the linking profiles.");
        return feebRepository.save(feeb);
    }


    public Feeb findFeeb(Feeb feeb)throws FeebException{
        if(null==feeb.getId() || (null==feeb.getClientProfileId() && null==feeb.getFeebProfileId()))
            throw new FeebException("Cannot link feeb. provide the linking profiles.");

        if(null!=feeb.getId())
            this.feeb=feebRepository.findById(feeb.getId());

        return this.feeb;
    }

//    public Feeb saveOrUpdateFeeb(Feeb feeb)throws FeebException{
//        if(null==feeb.getId() || null==feeb.getClientProfileId() || null==feeb.getFeebProfileId())
//            throw new FeebException("Cannot link feeb. provide the linking profiles.");
//
//        this.feeb = findFeeb(feeb);
//        if(this.feeb.getId()!=feeb.getId() ||
//           this.feeb.getClientProfileId()!=feeb.getClientProfileId() ||
//           this.feeb.getFeebProfileId()!=feeb.getFeebProfileId())
//                throw new FeebException("Cannot save. Feeb link does not match the feeb in db.");
//
//        return feebRepository.save(feeb);
//    }

    public void deleteFeeb(Feeb feeb){
        feebRepository.delete(feeb);
    }


}
