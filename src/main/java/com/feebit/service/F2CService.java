package com.feebit.service;

import com.feebit.domain.Feeb;
import com.feebit.exception.FeebException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Albert Vijai on 2/27/2015.
 */
@Service
public class F2CService {

    @Autowired
    FeebService feebService;
    @Autowired
    UserService userService;

    private Feeb feeb;

    /*
     *  TODO Notification, Authentication and other controls are not in scope'
     *  F2C-WorkFlow-Service. V0.0
     *  ===========================
     *  The Feeb Entity initiates the workflow by creating a feeb and linking it to a client
     *
     *  - Feeb entity creates a Feeb (clientEmail, clientScore)
     *  - Find clients default profile by email address
     *  - Create a client profile if profile is null
     *  - Link the feeb to client
     *  - Update Statistics for the client profile
     *  - Client responds and feeb is updated
     *  - Update Statistics for the feeb profile
     *
     */

    public void saveOrUpdateFeeb(String clientProfileId, String entityProfileId, String method) throws FeebException{

        //feebService.createFeeb(new ObjectId(clientProfileId), new ObjectId(clientProfileId));

    }


}
