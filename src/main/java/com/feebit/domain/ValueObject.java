package com.feebit.domain;

import org.bson.types.ObjectId;

/**
 * Created by yashureikhy on 2/21/15.
 */
public class ValueObject {

    private String _id;
    private double value;

    public String get_id() {
        return _id;
    }

    public void set_id(ObjectId profileId) {
        this._id = _id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ValueObject{" +
                "_id=" + _id +
                ", value=" + value +
                '}';
    }
}
