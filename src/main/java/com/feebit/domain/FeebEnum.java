package com.feebit.domain;

/**
 * Created by Albert Vijai on 2/18/2015.
 */
public class FeebEnum {

    public enum FeebMethod{C2F, F2C};
    public enum FeebType{A, B};
    public enum ProfileType{DEFAULT, PERSON, PRODUCT, BUSINESS};

}
