package com.feebit.domain;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Albert Vijai on 2/11/2015.
 */
@Document
public class Profile {

    @Id
    @Indexed
    private ObjectId id;
    @Indexed
    private ObjectId userId;
    private FeebEnum.ProfileType profileType;
    private String profileName, displayName, about, url;
    private boolean isEnabled, lock;
    @DBRef
    private List<Feeb> feebs;
    @DBRef
    private Statistics stats;


    public void addToUser(User user){
        if(null==user.getProfiles())
            user.setProfiles(new ArrayList<Profile>());

        user.getProfiles().add(this);
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public FeebEnum.ProfileType getProfileType() {
        return profileType;
    }

    public void setProfileType(FeebEnum.ProfileType profileType) {
        this.profileType = profileType;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public boolean isLock() {
        return lock;
    }

    public void setLock(boolean lock) {
        this.lock = lock;
    }

    public List<Feeb> getFeebs() {
        return feebs;
    }

    public void setFeebs(List<Feeb> feebs) {
        this.feebs = feebs;
    }

    public Statistics getStats() {
        return stats;
    }

    public void setStats(Statistics stats) {
        this.stats = stats;
    }
}
