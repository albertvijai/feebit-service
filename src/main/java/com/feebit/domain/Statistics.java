package com.feebit.domain;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Albert Vijai on 2/18/2015.
 */
@Document
public class Statistics {

    @Id
    private ObjectId id;
    @Indexed
    private ObjectId profileId;

    private Long n;
    private Double sum;
    private Double min;
    private Double max;
    private Double mean;
    private Double wtMean;

    public Statistics(ObjectId profileId){
        this.profileId = profileId;
    }



}
