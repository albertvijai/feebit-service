package com.feebit.domain;

import com.feebit.exception.FeebException;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Albert Vijai on 2/18/2015.
 */
@Document
public class Feeb {

    @Id
    @Indexed
    private ObjectId id;
    private Date createDate;
    private FeebEnum.FeebType feebType;
    private FeebEnum.FeebMethod feebMethod;

    @Indexed
    private ObjectId clientProfileId;
    private Date     clientUpdateDate;
    private Double   clientPct;
    private Double   clientUnitWt;
    private Double   clientWtPct;
    private boolean  clientLike;
    private Integer  clientCounter;

    @Indexed
    private ObjectId feebProfileId;
    private Date     feebUpdateDate;
    private Double   feebPct;
    private Double   feebUnitWt;
    private Double   feebWtPct;
    private boolean  feebLike;
    private Integer  feebCounter;

    public Feeb(ObjectId clientProfileId, ObjectId feebProfileId) throws FeebException{
        if(null==clientProfileId && null==feebProfileId)
            throw new FeebException("Cannot link feeb. provide the linking profiles.");

        this.clientProfileId = clientProfileId;
        this.feebProfileId = feebProfileId;
        this.createDate = new Date();
    }


    public void addToProfile(Profile profile){
        if(null==profile.getFeebs())
            profile.setFeebs(new ArrayList<Feeb>());

        profile.getFeebs().add(this);
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public FeebEnum.FeebType getFeebType() {
        return feebType;
    }

    public void setFeebType(FeebEnum.FeebType feebType) {
        this.feebType = feebType;
    }

    public FeebEnum.FeebMethod getFeebMethod() {
        return feebMethod;
    }

    public void setFeebMethod(FeebEnum.FeebMethod feebMethod) {
        this.feebMethod = feebMethod;
    }

    public ObjectId getClientProfileId() {
        return clientProfileId;
    }

    public void setClientProfileId(ObjectId clientProfileId) {
        this.clientProfileId = clientProfileId;
    }

    public Date getClientUpdateDate() {
        return clientUpdateDate;
    }

    public void setClientUpdateDate(Date clientUpdateDate) {
        this.clientUpdateDate = clientUpdateDate;
    }

    public Double getClientPct() {
        return clientPct;
    }

    public void setClientPct(Double clientPct) {
        this.clientPct = clientPct;
    }

    public Double getClientUnitWt() {
        return clientUnitWt;
    }

    public void setClientUnitWt(Double clientUnitWt) {
        this.clientUnitWt = clientUnitWt;
    }

    public Double getClientWtPct() {
        return clientWtPct;
    }

    public void setClientWtPct(Double clientWtPct) {
        this.clientWtPct = clientWtPct;
    }

    public boolean isClientLike() {
        return clientLike;
    }

    public void setClientLike(boolean clientLike) {
        this.clientLike = clientLike;
    }

    public Integer getClientCounter() {
        return clientCounter;
    }

    public void setClientCounter(Integer clientCounter) {
        this.clientCounter = clientCounter;
    }

    public ObjectId getFeebProfileId() {
        return feebProfileId;
    }

    public void setFeebProfileId(ObjectId feebProfileId) {
        this.feebProfileId = feebProfileId;
    }

    public Date getFeebUpdateDate() {
        return feebUpdateDate;
    }

    public void setFeebUpdateDate(Date feebUpdateDate) {
        this.feebUpdateDate = feebUpdateDate;
    }

    public Double getFeebPct() {
        return feebPct;
    }

    public void setFeebPct(Double feebPct) {
        this.feebPct = feebPct;
    }

    public Double getFeebUnitWt() {
        return feebUnitWt;
    }

    public void setFeebUnitWt(Double feebUnitWt) {
        this.feebUnitWt = feebUnitWt;
    }

    public Double getFeebWtPct() {
        return feebWtPct;
    }

    public void setFeebWtPct(Double feebWtPct) {
        this.feebWtPct = feebWtPct;
    }

    public boolean isFeebLike() {
        return feebLike;
    }

    public void setFeebLike(boolean feebLike) {
        this.feebLike = feebLike;
    }

    public Integer getFeebCounter() {
        return feebCounter;
    }

    public void setFeebCounter(Integer feebCounter) {
        this.feebCounter = feebCounter;
    }
}
